# Javascript2AST

Convert a javascript source file into an Abstract Syntax Tree (AST) which is an intermediate step to create an equivalent code in another programming language. The main underlying library is UglifyJS that create an AST e.g. for code compression.

## UglifyJS
The [parser in UglifyJS](http://lisperator.net/uglifyjs/ast) creates an [Abstract Syntax Tree - AST](https://en.wikipedia.org/wiki/Abstract_syntax_tree). Tree node visitor is called [Walker](http://lisperator.net/uglifyjs/walk) that walks over the nodes of the AST and could transform and extract information out of the code.

```javascript
var code = "function foo() {\n\
  function x(){}\n\
  function y(){}\n\
}\n\
function bar() {}";
var toplevel = UglifyJS.parse(code);
var walker = new UglifyJS.TreeWalker(function(node){
    if (node instanceof UglifyJS.AST_Defun) {
        // string_template is a cute little function that UglifyJS uses for warnings
        console.log(UglifyJS.string_template("Found function {name} at {line},{col}", {
            name: node.name.name,
            line: node.start.line,
            col: node.start.col
        }));
    }
});
toplevel.walk(walker);
```

## Commands for Repository
* `node ./bin/processfile.js --ast tests/files/braces.txt` takes the input file `tests/files/braces.txt` and creates the AST and outputs the AST with `JSON.stringify` to the console. You can replace your file with any other test file in the repository e.g.  `tests/files/function1.txt`. Add your own test code for creating the AST in the folder `tests/files` and process them with `node processfile.js`. 
* The following command lets `git` ignore all the node modules intalled by `npm install` of the repository so that downloaded NPM modules are not stored in your git repository.
    touch .gitignore && echo "node_modules/" >> .gitignore
