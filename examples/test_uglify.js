var UglifyJS = require('uglify-js');
var code = "function add(first, second) { return first + second; }";
var result = UglifyJS.minify(code);
//var result = UglifyJS.beautify(code);
console.log("ERROR: " + (result.error || "no errors")); // runtime error, or `undefined` if no error
console.log("CODE: " + result.code);  // minified output: function add(n,d)
