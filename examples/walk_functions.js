var UglifyJS = require('uglify-js')
var code1 = `function foo() {\n\
  function x(){}\n\
  function y(){}\n\
}\n\
// different function definition
var bar = function (px,py) { return px + py}
// end of file `;
var code2 = `
// different function definition
var bar = function (px,py) { return px + py}
// end of file `;
var toplevel = UglifyJS.parse(code2);
var vCount = 0 ;
var walker = new UglifyJS.TreeWalker(function(node){
  vCount++;
  if (node instanceof UglifyJS.AST_VarDef) {
      // string_template is a cute little function that UglifyJS uses for warnings
      console.log("Found AST_VarDef Node (" + vCount  + "): " + node.constructor.name + " at (Line:" + node.start.line + ",C"+ node.start.col +") " + JSON.stringify(node,null,4));
  } else {
    console.log("Instance of Node (" + vCount  + "): " + node.constructor.name+"\nNode" + vCount  + ": " + JSON.stringify(node,null,4));
  }
});
console.log("AST:\n" + JSON.stringify(toplevel,null,4));

toplevel.walk(walker);
