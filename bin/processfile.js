var UglifyJS = require('uglify-js')
var fs = require('fs');
var path = require('path');
//var vFileName = path.join(__dirname,'tests/cache/africaans.txt');
var vDefaultFile = './tests/files/default.js';
var vFileName = "";
var mode = 'ast';

function process_file(pMode,pCode) {
  switch (pMode) {
    case "ast":
      show_ast(pCode);
    break;
    case "minify":
      minify_code(pCode);
    break;
    case "nodetypes":
      show_nodetypes(pCode);
    break;
    case "nodes":
      show_nodes(pCode);
    break;
    default:
      show_ast(pCode);
  }
}

function show_ast(pCode) {
  var toplevel = UglifyJS.parse(pCode);
  console.log("AST:\n" + JSON.stringify(toplevel,null,4));
}

function select_nodes(pCode) {
  var toplevel = UglifyJS.parse(pCode);
  var vCount = 0 ;
  var walker = new UglifyJS.TreeWalker(function(node){
      vCount++;
      if (node instanceof UglifyJS.AST_VarDef) {
          // string_template is a cute little function that UglifyJS uses for warnings
          console.log("Found AST_VarDef Node (" + vCount  + "): " + node.constructor.name + " at (Line:" + node.start.line + ",C"+ node.start.col +") " + JSON.stringify(node,null,4));
      } else {
        console.log("Instance of Node (" + vCount  + "): " + node.constructor.name+"\nNode" + vCount  + " = " + JSON.stringify(node,null,4));
      }
  });
  toplevel.walk(walker);
}

function show_nodes(pCode) {
  var toplevel = UglifyJS.parse(pCode);
  var vCount = 0 ;
  var walker = new UglifyJS.TreeWalker(function(node){
      vCount++;
      console.log("Found Node (" + vCount  + "): " + node.constructor.name + " at (Line:" + node.start.line + ",C"+ node.start.col +") Node" + vCount + " = " + JSON.stringify(node,null,4));
  });
  toplevel.walk(walker);
}


function show_nodetypes(pCode) {
  var toplevel = UglifyJS.parse(pCode);
  var vCount = 0 ;
  var vLog = "";
  var walker = new UglifyJS.TreeWalker(function(node){
      vCount++;
      vLog = "Found Node (" + vCount  + "): " + node.constructor.name + " at (Line:" + node.start.line + ",C"+ node.start.col +")";
      vJSON = JSON.stringify(node,null,4);
      for (var variable in node) {
        if (node.hasOwnProperty(variable)) {
          vLog += " " + variable;
        }
      }
      console.log(vLog);
  });
  toplevel.walk(walker);
}

function minify_code(pCode) {
  var result = UglifyJS.minify(pCode);
  //var result = UglifyJS.beautify(code);
  console.log("ERROR: " + (result.error || "no errors")); // runtime error, or `undefined` if no error
  console.log("CODE: " + result.code);  // minified output: function add(n,d)
}

console.log("ARG-PARAMETER: ("+process.argv.join(",")+")");
var args = process.argv.slice(2, process.argv.length);
console.log("ARGS: ("+args.join(",")+")");
var modes = {
  '--ast': 'ast',
  '--nodetypes': 'nodetypes',
  '--js': 'js',
  '--minify': 'minify',

};
if (args.length < 1) {
  console.log("ERROR: missing parameter\n   node bin/processfile.js  --<mode> filename\n   modes: --ast --js  --nodetypes ");
} else if (args.length < 2) {
  console.log("CALL: processfile.js with default mode '--" + mode + "' and treat first parameter as filename.");
  vFileName = args[0];
} else {
  // processfile is called with less than 3 parameters
  // set the missing parameters with default values
  if (modes.hasOwnProperty(args[0]) === true) {
    mode = modes[args[0]];
    vFileName = args[1];
    console.log("(1) MODE: procesfile.js Mode was set to '"+mode+"'");
  } else {
    mode = "ast";
    vFileName = args[1];
    console.log("(1) MODE: processfile.js Mode use default mode '"+mode+"'");
  }
  console.log("FILE: processfile.js Mode set file to '"+vFileName+"'");
}
// read filename and apply wtf() on wiki data
fs.readFile(vFileName, {encoding:"utf-8"}, function(err,data){
  if (!err) {
    //console.log('Received Data from "'+vFileName+'":\n'+data);
    console.log("---BEGIN SOURCE----------\n" + data + "\n-----END SOURCE-------------");
    process_file(mode,data)
  } else {
    console.log("ERROR '"+vFileName+"':\n: "+err);
  }
});
